//
//  ViewController.h
//  mailSending
//
//  Created by cli-macmini-17 on 1/11/16.
//  Copyright © 2016 cli-macmini-17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ViewController : UIViewController
<MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *showEmail;
- (IBAction)showEmail:(id)sender;


@end

