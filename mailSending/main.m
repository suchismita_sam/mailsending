//
//  main.m
//  mailSending
//
//  Created by cli-macmini-17 on 1/11/16.
//  Copyright © 2016 cli-macmini-17. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
